import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DOMDocCreator {
    private DocumentBuilderFactory builderFactory;
    private DocumentBuilder documentBuilder;
    private Document document;

    public DOMDocCreator(File xml) {
        this.createDOMBuilder();
        this.createDoc(xml);
    }

    private void createDOMBuilder() {
        this.builderFactory = DocumentBuilderFactory.newInstance();

        try {
            this.documentBuilder = this.builderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException var2) {
            var2.printStackTrace();
        }

    }

    private void createDoc(File xml) {
        try {
            this.document = this.documentBuilder.parse(xml);
        } catch (IOException | SAXException var3) {
            var3.printStackTrace();
        }

    }

    public Document getDocument() {
        return this.document;
    }
}
