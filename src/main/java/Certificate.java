public class Certificate {
    private int numberOfRegistretion;
    private String registrationDate;
    private String expirationDate;
    private String nameOfOrganisation;

    public Certificate() {
    }

    public Certificate(int numberOfRegistretion, String registrationDate, String expirationDate,
                       String nameOfOrganisation) {
        this.numberOfRegistretion = numberOfRegistretion;
        this.registrationDate = registrationDate;
        this.expirationDate = expirationDate;
        this.nameOfOrganisation = nameOfOrganisation;
    }

    public int getNumberOfRegistretion() {
        return numberOfRegistretion;
    }

    public void setNumberOfRegistretion(int numberOfRegistretion) {
        this.numberOfRegistretion = numberOfRegistretion;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getNameOfOrganisation() {
        return nameOfOrganisation;
    }

    public void setNameOfOrganisation(String nameOfOrganisation) {
        this.nameOfOrganisation = nameOfOrganisation;
    }

    @Override
    public String toString() {
        return "Certificate [numberOfRegistretion=" + numberOfRegistretion + ", registrationDate=" + registrationDate
                + ", expirationDate=" + expirationDate + ", nameOfOrganisation=" + nameOfOrganisation + "]";
    }
}
