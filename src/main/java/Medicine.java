import java.util.ArrayList;
import java.util.List;

public class Medicine {
    private String name;
    private String pharm;
    private String group;
    private List<String> nameOfAnalogs = new ArrayList<String>();
    private List<String> versions = new ArrayList<String>();
    private Certificate certificate;
    private Dosage dosage;

    public Medicine() {

    }

    public Medicine(String name, String pharm, String group, List<String> nameOfAnalogs, List<String> versions,
                    Certificate certificate, Dosage dosage) {
        this.name = name;
        this.pharm = pharm;
        this.group = group;
        this.nameOfAnalogs = nameOfAnalogs;
        this.versions = versions;
        this.certificate = certificate;
        this.dosage = dosage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPharm() {
        return pharm;
    }

    public void setPharm(String pharm) {
        this.pharm = pharm;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public List<String> getNameOfAnalogs() {
        return nameOfAnalogs;
    }

    public void setNameOfAnalogs(List<String> nameOfAnalogs) {
        this.nameOfAnalogs = nameOfAnalogs;
    }

    public List<String> getVersions() {
        return versions;
    }

    public void setVersions(List<String> versions) {
        this.versions = versions;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public Dosage getDosage() {
        return dosage;
    }

    public void setDosage(Dosage dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return "Medicine [name=" + name + ", pharm=" + pharm + ", group=" + group + ", nameOfAnalogs=" + nameOfAnalogs
                + ", versions=" + versions + ", certificate=" + certificate + ", dosage=" + dosage + "]";
    }
}
