
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

class DOMDocReader {
    public DOMDocReader() {
    }

    public List<Medicine> readDoc(Document doc) {
        doc.getDocumentElement().normalize();
        List<Medicine> medicines = new ArrayList();
        NodeList nodeList = doc.getElementsByTagName("medicine");

        for(int i = 0; i < nodeList.getLength(); ++i) {
            Medicine medicine = new Medicine();
            Node node = nodeList.item(i);
            if (node.getNodeType() == 1) {
                Element element = (Element)node;
                medicine.setName(element.getAttribute("name"));
                medicine.setPharm(element.getAttribute("pharm"));
                medicine.setGroup(element.getAttribute("group"));
                List<String> nameOfAnalogs=this.getAnalog(element.getElementsByTagName("nameOfAnalog"));
                List<String> versions=this.getAnalog(element.getElementsByTagName("versions"));
                Certificate certificate=this.getCertificane(element.getElementsByTagName("cartificate"));
                Dosage dosage=this.getDosage(element.getElementsByTagName("dosage"));

            }
        }

        return medicines;
    }

    private List<String> getAnalog(NodeList nodes) {
        List<String> analogs = new ArrayList();
        if (nodes.item(0).getNodeType() == 1) {
            Element element = (Element)nodes.item(0);
            NodeList nodeList = element.getChildNodes();

            for(int i = 0; i < nodeList.getLength(); ++i) {
                Node node = nodeList.item(i);
                if (node.getNodeType() == 1) {
                    Element el = (Element)node;
                    analogs.add(new String(el.getTextContent()));
                }
            }
        }

        return analogs;
    }

    private Certificate getCertificane(NodeList nodes) {
        Certificate certificate = new Certificate();
        if (nodes.item(0).getNodeType() == 1) {
            Element element = (Element)nodes.item(0);
            certificate.setNumberOfRegistretion(Integer.parseInt(element.getTagName()));
            certificate.setRegistrationDate(element.getTagName());
            certificate.setExpirationDate(element.getTagName());
            certificate.setNameOfOrganisation(element.getTagName());
        }

        return certificate;
    }

    private Dosage getDosage(NodeList nodes) {
        Dosage dosage=new Dosage();
        if (nodes.item(0).getNodeType() == 1) {
            Element element = (Element)nodes.item(0);
            dosage.setDosage(element.getTagName());
            dosage.setFrequency(element.getTagName());
        }

        return dosage;
    }
}
